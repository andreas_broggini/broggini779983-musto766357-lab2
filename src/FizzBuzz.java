import java.lang.IllegalArgumentException;

public class FizzBuzz implements IfizzBuzz 
{

	@Override
	public String fizzbuzzSequence(int number) 
	{
		String str = new String();
		if(numberIsNegative(number))
			throw new IllegalArgumentException("non accetto numeri negativi");
		for(int currentNumber=1; currentNumber<=number; currentNumber++)
		{	
			if(isFizz(currentNumber))
				str = addFizz(str);
			if(isMultiple5(currentNumber))
				str = addBuzz(str);
			if(isMultiple7(currentNumber))
				str = addWoof(str);
			if(isNotMultiple(currentNumber))
				str += currentNumber;
			if(conditionAddBlankOnString(number, currentNumber))
				str = addBlank(str);
		}
		return str;
	}

	private String addWoof(String str) {
		str += "woof";
		return str;
	}

	private boolean isMultiple7(int currentNumber) {
		return currentNumber%7==0;
	}

	private boolean numberIsNegative(int number) {
		return number<0;
	}

	private boolean isNotMultiple(int currentNumber) {
		return !isMultiple3(currentNumber) && !isMultiple5(currentNumber);
	}

	private String addBlank(String str) {
		str += " ";
		return str;
	}

	private String addBuzz(String str) {
		str += "buzz";
		return str;
	}

	private String addFizz(String str) {
		str += "fizz";
		return str;
	}

	private boolean conditionAddBlankOnString(int number, int currentNumber) {
		return currentNumber != number;
	}

	private boolean isMultiple5(int currentNumber) {
		return currentNumber%5==0;
	}

	private boolean isMultiple3(int currentNumber) {
		return currentNumber%3==0;
	}
	
	private boolean contain3(int currentNumber) {
		String s=Integer.toString(currentNumber);
        for(int j = 0;  j< s.length(); ++j) {
            if (s.charAt(j)=='3')
                return true;
        }
        return false;
	}	
	
	private boolean isFizz(int currentNumber) {
		if(isMultiple3(currentNumber) && contain3(currentNumber))
			return  true;
		if(isMultiple3(currentNumber))
			return true;
		if(contain3(currentNumber))
			return true;
		return false;
	}

}
