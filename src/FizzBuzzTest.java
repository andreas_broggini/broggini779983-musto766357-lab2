import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import java.lang.IllegalArgumentException;


public class FizzBuzzTest{

	private FizzBuzz f;


	/**
	 * La chiamata al metodo fizzbuzzSequence con parametro intero 1,
	 * deve restituire la stringa "1".
	 */
	@Before
	public void createObj() {
		f = new FizzBuzz();
	}
	
	@Test
	public void fizzbuzzSequence1() {
		assertEquals("1",f.fizzbuzzSequence(1));
	}

	
	@Test
	public void fizzbuzzSequence2() {
		assertEquals("1 2",f.fizzbuzzSequence(2));
	}
	
	@Test
	public void fizzbuzzSequence3() {
		f.fizzbuzzSequence(3).endsWith("2 fizz");
	}
	
	@Test
	public void fizzbuzzSequence4() {
		f.fizzbuzzSequence(5).endsWith("4 buzz");
	}
	
	@Test
	public void fizzbuzzSequence5() {
		f.fizzbuzzSequence(6).endsWith(" fizz");
	}
	
	@Test
	public void fizzbuzzSequence6() {
		f.fizzbuzzSequence(10).endsWith("fizz buzz");
	}
	
	@Test
	public void fizzbuzzSequence7() {
		f.fizzbuzzSequence(15).endsWith(" fizzbuzz");
	}
	
	@Test
	public void fizzbuzzSequence8() {
		f.fizzbuzzSequence(100).endsWith("98 fizz buzz");
	}
	
	@Test
	public void fizzbuzzSequence9(){
		try
		{
			f.fizzbuzzSequence(-1);
		}
		catch(IllegalArgumentException e)
		{
			assertEquals("non accetto numeri negativi", e.getMessage());
		}
	}
	
	@Test
	public void fizzbuzzSequence10() {
		f.fizzbuzzSequence(7).endsWith(" woof");
	}
}
